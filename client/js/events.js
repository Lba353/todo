"use strict";

//$(function () {
//  // init Isotope
//  var $collection = $('.collection').isotope({
//    itemSelector: '.collection-item',
//    layoutMode: 'fitRows'
//  });
//  // filter functions
//  var filterFns = {
//    // show if number is greater than 50
//    numberGreaterThan50: function () {
//      var number = $(this).find('.number').text();
//      return parseInt(number, 10) > 50;
//    },
//    // show if name ends with -ium
//    ium: function () {
//      var name = $(this).find('.name').text();
//      return name.match(/ium$/);
//    }
//  };
//  // bind filter button click
//  $('.filters-button-group').on('click', 'button', function () {
//    var filterValue = $(this).attr('data-filter');
//    // use filterFn if matches value
//    filterValue = filterFns[filterValue] || filterValue;
//    $collection.isotope({ filter: filterValue });
//  });
//  // change is-checked class on buttons
//  $('.button-group').each(function (i, buttonGroup) {
//    var $buttonGroup = $(buttonGroup);
//    $buttonGroup.on('click', 'button', function () {
//      $buttonGroup.find('.is-checked').removeClass('is-checked');
//      $(this).addClass('is-checked');
//    });
//  });
//  
//});



Template.form.events({
  'submit .add-new-task': function (event) {
    event.preventDefault();
    
    var taskName = event.currentTarget.children[0].firstElementChild.value;
    Collections.Todo.insert({
      name: taskName,
      createdAt: new Date(),
      complete: false,
      active: true
    });
    
    event.currentTarget.children[0].firstElementChild.value = "";
    return false;
    
  }
  
  
});

Template.todosIf.events({
  
  'click .delete-task': function (event) {
    Collections.Todo.remove({_id: this._id});
  },

  'click .complete-task': function (event) {
    Collections.Todo.update({_id: this._id}, {$set: {complete: true, active: false}});
  },

  'click .incomplete-task': function (event) {
    Collections.Todo.update({_id: this._id}, {$set: {complete: false, active: true}});
  },
  
  'click .revise-task': function (event, taskName) {
    var new_text = Collections.Todo.findOne({_id: this._id}, {name: 1}).name;
    var new_text = prompt(new_text, new_text);
    Collections.Todo.update({_id: this._id}, {$set: {name: new_text}});
  }

});