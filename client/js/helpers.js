"use strict";

Template.todosIf.helpers({
  todoList: function () {
    return Collections.Todo.find({});
  }
});

Template.todos.helpers({
  todoList: function () {
    return Collections.Todo.find({}).count();
  }
});